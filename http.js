const http=require('http')
const fs=require('fs')
const Url=require('url')
const uuid=require('uuid4')

function read(path){
    return new Promise((resolve,reject)=>{
        fs.readFile(path,'utf8',(err,data)=>{
            if(err){
                reject('error while reading html',err)
            }
            else{
                resolve(data)
            }
        })
    })
}

http.createServer((req,res)=>{
    if(req.method!=='GET'){
        res.end(`${req.method} method not allowed`)
    }
    else{
        let url='/'+req.url.split('/')[1]
        res.on('error',(err)=>{
            console.log(err.message)
        })
        switch(url){
            case '/html':{
                read('http.html')
                .then((data)=>{
                    res.write(data)
                    res.end()
                })
                .catch((err)=>{
                    console.log(err)
                })
                break
            }
            case '/json':{
                read('http.json')
                .then((data)=>{
                    res.write(data)
                    res.end()
                })
                .catch((err)=>{
                    console.log(err)
                })
                break
            }
            case '/uuid':{
                let id={"uuid":uuid()}
                res.write(JSON.stringify(id))
                res.end()
                break
            }
            case '/status':{
                let url1=req.url
                if(/\/status\/\d*$/.test(url1)){
                    let urlArr=url1.split('/')
                    if(Number(urlArr[2])>=1000 || Number(urlArr[2])<200){
                        res.end(`${urlArr[2]} status code doesn't exists`)
                    }
                    else{
                        res.writeHead(Number(urlArr[2]),{'Content-Type':'text/plain'})
                        res.end(`This response is with user given status code of ${urlArr[2]}`)
                    }
                }
                else{
                    res.write('url is not as per the specifications')
                    res.end()
                }
                break
            }
            case '/delay':{
                let url1=req.url
                if(/delay\/\d*$/.test(url1)){
                    let urlArr=url1.split('/')
                    if(Number(urlArr[2])>10000 ||Number(urlArr[2])<0){
                        res.end('delay should in between 0-10 seconds')
                    }
                    else{
                        let delay=new Promise((resolve,reject)=>{
                            setTimeout(()=>{
                                resolve(`This response is after delay by ${urlArr[2]}ms`)
                            },Number(urlArr[2]))
                        })
                        delay.then((data)=>{
                            res.write(data)
                            res.end()
                        })
                    }
                }
                else{
                    res.write('url is not as per specifications')
                    res.end()
                }
                break
            }
            default:{
                res.end('Try any of these /html,/json,/uuid,/delay/time,/status/code end points')
            }
        }
    }
}).listen(8003)